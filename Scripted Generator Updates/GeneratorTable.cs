﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;

namespace SeasonalUpdater
{
    class Generators
    {
        public List<Weenie> weenieList = null;
        private GenListConfig genConfig= null;
        private List<KeyValuePair<int, GeneratorTable>> weenieGenSlotToAdd = new List<KeyValuePair<int, GeneratorTable>>();

        public Generators(List<Weenie> weens)
        {
            this.weenieList = weens;
            this.genConfig = GenListConfigObjectFromString("..\\config\\GeneratorListConfig.json");
            this.LoadGenProfilesFromCSV();
        }

        public void AddNewItemToGenTable()
        {
            //TODO : This could stand for some refactoring...
            foreach (Weenie generator in weenieList)
            {
                List<GeneratorTable> newTableEntries = new List<GeneratorTable>();
                newTableEntries = weenieGenSlotToAdd.Where(CSVentries => CSVentries.Key == (int)generator.WeenieClassId).Select(CSVentry => CSVentry.Value).ToList();

                if (newTableEntries.Count() == 0)
                {
                    Console.WriteLine($"WARNING: No new entries found for {generator.WeenieClassId} - {generator.Name} ... SKIPPING.");
                    continue;
                }

                //Setup some configurations for dynamic slot probabilities.
                double aggregateProb = 0;
                double averageGenSlotProb = generator.GeneratorTable.Count > 0 ? generator.GeneratorTable.Select(slot => {
                    double normalizedSlotProb = slot.Probability - aggregateProb;
                    aggregateProb = slot.Probability;
                    return normalizedSlotProb;
                }).Average() : 0;

                double squishFactor = 0;
                if (genConfig.useGeneratorTableAvgProb)
                {
                    Console.WriteLine($"Using average generator table probability (prob = {averageGenSlotProb}) for new slot entry and squishing table entries by the same.");
                    squishFactor = averageGenSlotProb * newTableEntries.Count();
                }
                else if (genConfig.useGeneratorTableAvgProbOverride)
                {
                    foreach (GeneratorTable slot in newTableEntries)
                    {
                        if (slot.Probability > averageGenSlotProb)
                        {
                            Console.WriteLine($"Using average generator table probability as fixed prob ({slot.Probability}) is greater than average found in table ({averageGenSlotProb}).");
                            slot.Probability = averageGenSlotProb;
                        }
                    }
                    if (generator.WeenieClassId == 30427)
                    {
                        var name12 = generator.Name;
                    }

                    squishFactor = newTableEntries.Count() == 1 ? newTableEntries.ElementAt(0).Probability : newTableEntries.Select(x => x.Probability).Sum();
                }
                else
                {
                    Console.WriteLine($"Using fixed probabilities found in CSV. NOTE: Average table prob is {averageGenSlotProb}");
                    squishFactor = newTableEntries.Count() == 1 ? newTableEntries.ElementAt(1).Probability : newTableEntries.Select(slot => slot.Probability).Sum();
                }

                //squish existing entries              
                generator.GeneratorTable.ForEach(genSlot => genSlot.Probability *= 1 - squishFactor);

                //Add new slots POST configuration.
                string changeSummary = "";
                for (int listCount = 0; listCount < newTableEntries.Count(); listCount++)
                {
                    GeneratorTable slot = newTableEntries.ElementAt(listCount);
                    if ((listCount + 1 == newTableEntries.Count()))
                    {
                        slot.Probability = 1.0; //Last item in list. Set to max to avoid 0.9999 rounding issues.
                    }
                    else
                    {
                        slot.Probability = generator.GeneratorTable.ElementAt(generator.GeneratorTable.Count()-1).Probability + slot.Probability;
                    }

                    slot.Slot = (uint)generator.GeneratorTable.Count();
                    generator.GeneratorTable.Add(slot);

                    
                    if (genConfig.userChangeSummary.Equals(""))
                        changeSummary += $"-Added {slot.WeenieClassId} to generator table.{Environment.NewLine}";
                    else
                        changeSummary = genConfig.userChangeSummary;

                    generator.UserChangeSummary = changeSummary;

                    Console.WriteLine($"Added new slot {slot.Slot} to generator Table on weenie: {generator.WeenieClassId} - {generator.Name}{Environment.NewLine}");
                }
            }
            Console.WriteLine($"-----------------------{Environment.NewLine}All weenies parsed for updated generator tables.{Environment.NewLine}");
        }

        private GenListConfig GenListConfigObjectFromString(string path)
        {
            string configString = "";
            try
            {
                configString = File.ReadAllText(path);
                if (configString.Equals(""))
                    Console.WriteLine($"{Path.GetFileName(path)} is an empty file.{Environment.NewLine}");
                try
                {
                    GenListConfig config = JsonConvert.DeserializeObject<GenListConfig>(configString);
                    return config;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.GetBaseException().Message}{Environment.NewLine}Corrupted config file...{Path.GetFileName(path)}{Environment.NewLine}{Environment.NewLine}");
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.GetBaseException().Message}{Environment.NewLine}Corrupted config file...{Path.GetFileName(path)}{Environment.NewLine}{Environment.NewLine}");
                return null;
            }
        }

        private void LoadGenProfilesFromCSV()
        {
            Console.WriteLine($"Loading generator entries from CSV...please wait.");
            weenieGenSlotToAdd = File.ReadLines($"..\\config\\{genConfig.seasonalCSV}.csv").Skip(2).Select(line => line.Split(',')).Select(line => {
                GeneratorTable generatorSlotToAdd = new GeneratorTable();
                int generatorWCID = 0;
                try
                {
                    generatorWCID = int.Parse(line[1]);
                    generatorSlotToAdd.WeenieClassId = uint.Parse(line[3]);
                    generatorSlotToAdd.Delay = float.Parse(line[4]);
                    generatorSlotToAdd.Probability = double.Parse(line[5]);
                    generatorSlotToAdd.InitCreate = int.Parse(line[6]);
                    generatorSlotToAdd.MaxNumber = int.Parse(line[7]);
                    generatorSlotToAdd.StackSize = int.Parse(line[8]);
                    generatorSlotToAdd.WhenCreate = uint.Parse(line[9]);
                    generatorSlotToAdd.WhereCreate = uint.Parse(line[10]);
                    generatorSlotToAdd.Shade = float.Parse(line[11]);
                    generatorSlotToAdd.PaletteId = uint.Parse(line[12]);
                    generatorSlotToAdd.ObjectCell = uint.Parse(line[13]);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"FAILED to parse slot data for: {line.ToString()} | {e.Message}");
                }

                if(!line[14].Equals("0"))
                 ParseFrame(generatorSlotToAdd, line[14], generatorWCID);

                KeyValuePair<int, GeneratorTable> weenieAndItem = new KeyValuePair<int, GeneratorTable>(generatorWCID, generatorSlotToAdd);
                return weenieAndItem;
            }).ToList();

        }

        private void ParseFrame(GeneratorTable slot, string frameString, int weenieErrorWCID = 0)
        {
            frameString = frameString.Trim();
            frameString = frameString.Replace("[", "");
            frameString = frameString.Replace("]", "");
            string[] frameArray = frameString.Split(' ');
            try
            {
                slot.Frame.Position.X = float.Parse(frameArray[0]);
                slot.Frame.Position.Y = float.Parse(frameArray[1]);
                slot.Frame.Position.Z = float.Parse(frameArray[2]);

                slot.Frame.Rotations.W = float.Parse(frameArray[3]);
                slot.Frame.Rotations.X = float.Parse(frameArray[4]);
                slot.Frame.Rotations.Y = float.Parse(frameArray[5]);
                slot.Frame.Rotations.Z = float.Parse(frameArray[6]);
            }
            catch (Exception e)
            {
                Console.WriteLine($"FAILED to parse frame for: {weenieErrorWCID} | {e.Message}");
            }
        }

    }
}
