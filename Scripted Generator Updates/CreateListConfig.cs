﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SeasonalUpdater
{
    class CreateListConfig
    {
        [JsonProperty("seasonalCSVfilename")]
        public string seasonalCSV { get; set; }

        [JsonProperty("createCompanionEntries")]
        public bool companionEntries { get; set; }

        [JsonProperty("user_Change_Summary")]
        public string userChangeSummary { get; set; }
    }
}
