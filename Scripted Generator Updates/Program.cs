using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;

namespace SeasonalUpdater
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Welcome to Seasonal Weenie Updater! v1.0 {Environment.NewLine}Begin by entering directory from which to load (leave blank for default):");
            string directory = "";
            List<Weenie> weenieList = null;
            while (weenieList == null)
            {
                directory = Console.ReadLine();
                while (directory.Equals(""))
                {
                    Console.WriteLine("Path is empty. Using default path.");
                    directory = "..//InputWeenies";
                }

                weenieList = Load(directory);
            }

            List<string> caseList = CaseList();
            Console.WriteLine($"{Environment.NewLine}Select what action to execute:{Environment.NewLine}{String.Join(String.Empty, caseList.ToArray())}");

            int action = 0;
            while (action == 0 || action > caseList.Count)
            {
                if (!int.TryParse(Console.ReadLine(), out action))
                {
                    Console.WriteLine("Please use integer value assigned to action.");
                }
                else if (action > caseList.Count)
                {
                    Console.WriteLine("Invalid entry. No action assigned to that value! Try again.");
                }
            }

            switch ((Action)action)
            {
                case Action.CreateListAddition:
                    {
                        CreateLists lists = new CreateLists(weenieList);
                        lists.AddItemToCreateList();
                        ExportToFileAll(lists.weenieList);
                        break;
                    }
                case Action.GeneratorAddition:
                    {
                        Generators gens = new Generators(weenieList);
                        gens.AddNewItemToGenTable();
                        ExportToFileAll(gens.weenieList);
                        break;
                    }
                default:
                    break;
            }

            Console.WriteLine("End of program");
            Console.ReadKey();
        }

        public static List<Weenie> Load(string path)
        {
            //enumerate files in path

            String[] fileEntries = null;
            string loadErrors = "";
            try
            {
                fileEntries = Directory.GetFiles(path, "*.json");
            }
            catch (DirectoryNotFoundException e)
            {
                loadErrors += $"{e.Message} {Environment.NewLine}Please try again.";
            }
            catch (IOException e)
            {
                loadErrors += $"{e.Message} {Environment.NewLine}Please try again.";
            }
            catch (Exception e)
            {
                loadErrors += $"{e.GetBaseException().Message}{Environment.NewLine}Please try again.";
            }

            if (fileEntries != null && fileEntries.Length == 0)
            {
                loadErrors += "Path directory is empty. Aborting...";
            }

            if (!loadErrors.Equals(""))
            {
                Console.WriteLine(loadErrors);
                return null;
            }

            List<Weenie> weenieList = new List<Weenie>();

            Parallel.ForEach<string>(fileEntries, (file) =>
            {
                Weenie jsonObject = JObjectFromString(JSONStringFromFile(file), file);

                if (jsonObject != null)
                {
                    weenieList.Add(jsonObject);
                }

            });

            Console.WriteLine($"Total weenies in list: {weenieList.Count}");

            return weenieList;
        }

        public static string JSONStringFromFile(string path)
        {
            string weenieString = "";
            try
            {
                weenieString = File.ReadAllText(path);
                if (weenieString.Equals(""))
                    Console.WriteLine($"{Path.GetFileName(path)} is an empty file.{Environment.NewLine}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.GetBaseException().Message}{Environment.NewLine}Skipping File...{Path.GetFileName(path)}{Environment.NewLine}{Environment.NewLine}");
            }
            return weenieString;
        }

        public static Weenie JObjectFromString(string jsonString, string path)
        {
            if (jsonString.Equals(""))
                return null;
            try
            {
                Weenie weenie = JsonConvert.DeserializeObject<Weenie>(jsonString);
                return weenie;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.GetBaseException().Message}{Environment.NewLine}Skipping File...{Path.GetFileName(path)}{Environment.NewLine}{Environment.NewLine}");
                return null;
            }
        }

        public static void ExportToFileAll(List<Weenie> modifiedWeenieList)
        {
            Console.WriteLine("Exporting All to file.");
            foreach (Weenie _weenie in modifiedWeenieList)
            {
                string fileName = $"{_weenie.WeenieClassId} - {_weenie.Name}.json";
                string path = $"..\\ExportWeenies\\{fileName}";
                string _weenieString = JsonConvert.SerializeObject(_weenie, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

                File.WriteAllText(path, _weenieString);

            }
            Console.WriteLine("Export complete.");
        }

        public static List<string> CaseList()
        {
            List<string> caseList = new List<string>();

            caseList.Add($"Case 1: Add entry to CreateList.{Environment.NewLine}");
            caseList.Add($"Case 2: Add entry to generator.{Environment.NewLine}");
            return caseList;
        }

        public enum Action
        {
            CreateListAddition = 1,
            GeneratorAddition = 2
        }
    }
}
