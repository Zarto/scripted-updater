﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;

namespace SeasonalUpdater
{
    class CreateLists
    {
        private List<KeyValuePair<int,CreateItem>> weenieItemToAdd = new List<KeyValuePair<int,CreateItem>>();
        public List<Weenie> weenieList = null;
        private CreateListConfig createConfig = null;

        public CreateLists(List<Weenie> weens)
        {
            this.weenieList = weens;
            this.createConfig = CreateListConfigObjectFromString("..\\config\\CreateListConfig.json");
            this.LoadWeenieToItemCSV();
        }

        public void AddItemToCreateList()
        {
            foreach (Weenie weenie in weenieList)
            {
                bool entryNotFound = true;
                string changeSummary = "";
                foreach (KeyValuePair<int,CreateItem> keyValue in weenieItemToAdd)
                {
                    if (keyValue.Key == weenie.WeenieClassId)
                    {
                        entryNotFound = false;
                        if (weenie.CreateList == null)
                        {
                            Console.WriteLine($"WARNING: {weenie.WeenieClassId} - {weenie.Name} does not contain a createList! Adding createList.");
                            weenie.CreateList = new List<CreateItem>();
                        }

                        weenie.CreateList.Add(keyValue.Value);

                        string blankMsg = "";
                        if (createConfig.companionEntries)
                        {
                            CreateItem blankCreateListEntry = new CreateItem();
                            blankCreateListEntry.WeenieClassId = 0;
                            blankCreateListEntry.Palette = 0;
                            blankCreateListEntry.Shade = 1 - keyValue.Value.Shade;
                            blankCreateListEntry.Destination = keyValue.Value.Destination;
                            blankCreateListEntry.StackSize = 0;
                            blankCreateListEntry.TryToBond = 0;
                            weenie.CreateList.Add(blankCreateListEntry);
                            blankMsg = " and blank";
                        }
                        
                        if (createConfig.userChangeSummary.Equals(""))
                            changeSummary += $"*Added {keyValue.Value.WeenieClassId}{blankMsg} entry to createList.{Environment.NewLine}";  
                        else
                            changeSummary = createConfig.userChangeSummary;

                        Console.WriteLine($"Successfully added createItem{blankMsg} entry to {weenie.WeenieClassId} - {weenie.Name}");
                    }
                }
                if(!changeSummary.Equals(""))
                    weenie.UserChangeSummary = changeSummary;
                if (entryNotFound)
                {
                    Console.WriteLine($"WARNING: Unable to locate a create list addition for {weenie.WeenieClassId} - {weenie.Name}");
                }
            }

            Console.WriteLine($"-----------------------{Environment.NewLine}All weenies parsed for updated createLists.{Environment.NewLine}");
        }

        private void LoadWeenieToItemCSV()
        {
            Console.WriteLine($"Loading create entries from CSV...please wait.");
            weenieItemToAdd = File.ReadLines($"..\\config\\{createConfig.seasonalCSV}.csv").Skip(1).Select(line => line.Split(',')).Select(line => {
                CreateItem createListWeenieToAdd = new CreateItem();
                createListWeenieToAdd.WeenieClassId = uint.Parse(line[3]);
                createListWeenieToAdd.Palette = uint.Parse(line[4]);
                createListWeenieToAdd.Shade = double.Parse(line[5]);
                createListWeenieToAdd.Destination = uint.Parse(line[6]);
                createListWeenieToAdd.StackSize = int.Parse(line[7]);
                createListWeenieToAdd.TryToBond = byte.Parse(line[8]);
                KeyValuePair<int, CreateItem> weenieAndItem = new KeyValuePair<int, CreateItem>(int.Parse(line[1]), createListWeenieToAdd);
                return weenieAndItem;
                }).ToList();

        }

        private CreateListConfig CreateListConfigObjectFromString(string path)
        {
            string configString = "";
            try
            {
                configString = File.ReadAllText(path);
                if (configString.Equals(""))
                    Console.WriteLine($"{Path.GetFileName(path)} is an empty file.{Environment.NewLine}");
                try
                {
                    CreateListConfig config = JsonConvert.DeserializeObject<CreateListConfig>(configString);
                    return config;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.GetBaseException().Message}{Environment.NewLine}Corrupted config file...{Path.GetFileName(path)}{Environment.NewLine}{Environment.NewLine}");
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.GetBaseException().Message}{Environment.NewLine}Corrupted config file...{Path.GetFileName(path)}{Environment.NewLine}{Environment.NewLine}");
                return null;
            }
        }
    }
}
