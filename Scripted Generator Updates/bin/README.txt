==========================================================
=		Seasonal Weenie Updater			 =
==========================================================

Premise:
	This tool fills the gap of allowing one set of data to be maintained (LSD) and allow seasonal changes to take advantage of evolving data on the base weenies. This utility takes the fixed (yet data driven) seasonal items and re-applies them to a fresh import of LSD data for that new season.

HowTo:

	1. Before executing configure batch files/config files in config folder.
	2. For each batch file define directory path for SOURCE. DESTINATION defaults to weenieInputs.
	3. Execute only one batch file at a time. Can only execute createList changes or generator changes for any one execution. Not both simultaneously. These batch files bulk copy needed weenie files to program input directory.
	4. CSV files for both generatorTables and createLists are included for the fall festival and contain needed modifications. Additional modifications maybe added as needed. This data is pulled in by the program during execution and used to define what is added to what weenie. Additional CSVs can be added for other seasons and defined for use in CreateListConfig.json or GeneratorListConfig.json
	5. Additional configuration options can be found in CreateListConfig.json or GeneratorListConfig.json and are defined there.