::~~~~~~~~~Batch Support for copying of weenies requiring createList modification for various seasonal changes~~~~~~~~~~~~~~
:: Please visit the following spreadsheet for more info: https://docs.google.com/spreadsheets/d/1gLvNwYRDYQtnlhjHgAhk1nZoczluv0BHtOs99n2mn9c/edit?usp=sharing
@Echo off
DEL BatchExecutionLog.log
SET LOGFILE=BatchExecutionLog.log
SET DESTINATION=%~dp0..\InputWeenies
::!!!!!!!SET FOLLOWING LOCAL PARAMS!!!!!!!!!!
SET SOURCE=D:\Appz\Asheron's Call Backup Data\GDLE\GDLESERVER\Bin\Data\json\weenies\BBLSD

del "%DESTINATION%\*.json" /q
@echo Setup complete
@echo Press enter to continue...
call :Logit >> %LOGFILE%


:Logit
@echo on
:: COPY spreadsheet list below this line.
::-------------------------------------------
copy "%SOURCE%\5761 - Snowman.json" "%DESTINATION%"
copy "%SOURCE%\5761 - Snowman.json" "%DESTINATION%"
copy "%SOURCE%\5765 - Snowman.json" "%DESTINATION%"
copy "%SOURCE%\5765 - Snowman.json" "%DESTINATION%"
copy "%SOURCE%\5766 - Snowman.json" "%DESTINATION%"
copy "%SOURCE%\5766 - Snowman.json" "%DESTINATION%"
copy "%SOURCE%\5767 - Giant Snowman.json" "%DESTINATION%"
copy "%SOURCE%\7989 - Scavenger Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7989 - Scavenger Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7989 - Scavenger Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7989 - Scavenger Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7989 - Scavenger Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7989 - Scavenger Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7990 - Field Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7990 - Field Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7990 - Field Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7993 - Linvak Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7993 - Linvak Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7993 - Linvak Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7994 - Dire Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7994 - Dire Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\7994 - Dire Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\8270 - Sotiris Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\10787 - Terebrous Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\10789 - Terebrous Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\12681 - Minion Leader.json" "%DESTINATION%"
copy "%SOURCE%\14466 - Two Headed Snowman.json" "%DESTINATION%"
copy "%SOURCE%\16914 - Terebrous Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\23555 - Telumiat Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\23568 - Dreadful Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\23568 - Dreadful Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\23568 - Dreadful Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\27417 - Telumiat Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\27497 - Telumiat Hollow Minion.json" "%DESTINATION%"
copy "%SOURCE%\27715 - Ferocious Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\27715 - Ferocious Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\27715 - Ferocious Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\27716 - Raging Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\27716 - Raging Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\27716 - Raging Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\28644 - Frenzied Fiun.json" "%DESTINATION%"
copy "%SOURCE%\28658 - Sycophantic Penguin.json" "%DESTINATION%"
copy "%SOURCE%\28660 - Uber Penguin.json" "%DESTINATION%"
copy "%SOURCE%\28661 - Uber Penguin.json" "%DESTINATION%"
copy "%SOURCE%\28662 - Penguin.json" "%DESTINATION%"
copy "%SOURCE%\28663 - Arrogant Penguin.json" "%DESTINATION%"
copy "%SOURCE%\28664 - Augmented Penguin.json" "%DESTINATION%"
copy "%SOURCE%\28666 - Ruschk Slayer.json" "%DESTINATION%"
copy "%SOURCE%\28668 - Ruschk Warlord.json" "%DESTINATION%"
copy "%SOURCE%\28669 - Barbaric Ruschk.json" "%DESTINATION%"
copy "%SOURCE%\29342 - Ruschk Laktar.json" "%DESTINATION%"
copy "%SOURCE%\29344 - Ruschk Sadist.json" "%DESTINATION%"
copy "%SOURCE%\38181 - Blighted Dire Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\38181 - Blighted Dire Ursuin.json" "%DESTINATION%"
copy "%SOURCE%\38181 - Blighted Dire Ursuin.json" "%DESTINATION%"
::-------------------------------------------
:: COPY spreadsheet list above this line.
@echo ----------
@echo File copy complete.
@echo off
pause

