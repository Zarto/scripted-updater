::~~~~~~~~~Batch Support for copying of weenies requiring generator table modification for various seasonal changes~~~~~~~~~~~~~~
:: Please visit the following spreadsheet for more info: https://docs.google.com/spreadsheets/d/1gLvNwYRDYQtnlhjHgAhk1nZoczluv0BHtOs99n2mn9c/edit?usp=sharing
@Echo off
DEL BatchExecutionLog.log
SET LOGFILE=BatchExecutionLog.log
SET DESTINATION=%~dp0..\InputWeenies
::!!!!!!!SET FOLLOWING LOCAL PARAM!!!!!!!!!!
SET SOURCE=D:\Appz\Asheron's Call Backup Data\GDLE\GDLESERVER\Bin\Data\json\weenies\BBLSD

del "%DESTINATION%\*.json" /q
@echo Setup complete
@echo Press enter to continue...
call :Logit >> %LOGFILE%


:Logit
@echo on
:: COPY spreadsheet list below this line.
::-------------------------------------------
copy "%SOURCE%\23159 - Mid North Plains Generator.json" "%DESTINATION%"
copy "%SOURCE%\23160 - Mid North Plains Monouga Generator.json" "%DESTINATION%"
copy "%SOURCE%\23161 - Mid North Plains Tumerok Generator.json" "%DESTINATION%"
copy "%SOURCE%\23151 - Mid North Forest Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\23153 - Mid North Mountains Banderling Generator.json" "%DESTINATION%"
copy "%SOURCE%\23154 - Mid North Mountains Drudge Generator.json" "%DESTINATION%"
copy "%SOURCE%\23156 - Mid North Mountains Golem Generator.json" "%DESTINATION%"
copy "%SOURCE%\23157 - Mid North Mountain Mattekar Generator.json" "%DESTINATION%"
copy "%SOURCE%\23158 - Mid North Mountains Tusker Generator.json" "%DESTINATION%"
copy "%SOURCE%\23155 - Mid North Mountains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21184 - Low North Inland Shore Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\23146 - Low North Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21183 - Low North Forest Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21185 - Low North Mountain Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21186 - Low North Golem Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21187 - Low North Golem Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21174 - Low Central Swamp Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21175 - Low Central Mosswart Swamp Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21176 - Low Central Sclavus Swamp Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\7930 - Low Central Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\7931 - Low Central Reedshark Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21171 - Low Central Shadow Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21172 - Low Central Undead Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\4623 - Low Central Forest Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\7890 - Low Central Mountain Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\7927 - Low Central Lugian Mountain Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\7928 - Low Central Mattekar Mountain Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\7929 - Low Central Ursuin Mountain Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21177 - Low Central Desert Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21178 - Low Central Desert Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21181 - Low Central Desert Skeleton Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21182 - Low Central Desert Undead Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21177 - Low Central Desert Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\21179 - Low Central Desert Reavers Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\23148 - Mid Central Plains Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\23149 - Mid Central Tumerok Mix Generator.json" "%DESTINATION%"
copy "%SOURCE%\23165 - Mid South Lakes Generator.json" "%DESTINATION%"
copy "%SOURCE%\23172 - Mid South Plains Generator.json" "%DESTINATION%"
copy "%SOURCE%\23164 - Mid South Forest Generator.json" "%DESTINATION%"
copy "%SOURCE%\23166 - Mid SOuth Mountains Generator.json" "%DESTINATION%"
copy "%SOURCE%\23167 - Mid South Mountains Lugian Generator.json" "%DESTINATION%"
copy "%SOURCE%\23168 - Mid South Mountains Mattekar Generator.json" "%DESTINATION%"
copy "%SOURCE%\23169 - Mid South Mountains Undead Generator.json" "%DESTINATION%"
copy "%SOURCE%\23170 - Mid South Mountains Ursuin Generator.json" "%DESTINATION%"
copy "%SOURCE%\23171 - Mid South Mountains Virindi Generator.json" "%DESTINATION%"
copy "%SOURCE%\5150 - Harmless Aluvian Generator.json" "%DESTINATION%"
copy "%SOURCE%\5149 - Harmless Sho Generator.json" "%DESTINATION%"
copy "%SOURCE%\5151 - Harmless Gharun Generator.json" "%DESTINATION%"
copy "%SOURCE%\30427 - Harmless Viamontian Gen.json" "%DESTINATION%"
::-------------------------------------------
:: COPY spreadsheet list above this line.
@echo ----------
@echo File copy complete.
@echo off
pause

