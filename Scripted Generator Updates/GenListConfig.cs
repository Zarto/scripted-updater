﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;

namespace SeasonalUpdater
{
    class GenListConfig
    {
        [JsonProperty("Use_GeneratorTable_Average_Slot_Probability")]
        public bool useGeneratorTableAvgProb { get; set; }

        [JsonProperty("Use_GeneratorTable_Average_Slot_Probability_Override")]
        public bool useGeneratorTableAvgProbOverride { get; set; }

        [JsonProperty("seasonalCSVfilename")]
        public string seasonalCSV { get; set; }

        [JsonProperty("user_Change_Summary")]
        public string userChangeSummary { get; set; }

    }
}